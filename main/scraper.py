# Author: Kaan Eraslan
# purpose: Scrape images from CDLI
# No warranties, see LICENSE

import requests
import json
import datetime
import time
import pdb
import re
from PIL import Image
from io import BytesIO
import os
import glob
from uuid import uuid4


class ResultsParser:
    def __init__(self, results: str,
                 metadata_path: str,
                 transliteration_path: str):
        self.raw_results = results
        self.results = results.split('Primary publication')
        self.results = ['Primary publication' + res for res in self.results]
        self.results = self.results[1:]  # get rid of first Primary publication
        self.results = [r for r in self.results if r]
        # pdb.set_trace()
        self.metadata_path = metadata_path
        self.transliteration_path = transliteration_path

    def getRawTransliteration(self, result: str):
        "Get transliteration from result"
        transpos = result.find('Transliteration:')
        return result[transpos:]

    def stripTransliteration(self, result: str):
        "Strip transliteration from result"
        transpos = result.find('Transliteration:')
        return result[:transpos]

    def getKeyFromMetadata(self, metadata: str):
        "Get metada key"
        colpos = metadata.find(':')
        return metadata[:colpos].strip()

    def getValueFromMetadata(self, metadata: str):
        "Get metadata value"
        colpos = metadata.find(':')
        return metadata[colpos+1:].strip()

    def getKeyValFromMetadata(self, metadata: str):
        "Get metadata key and value"
        key = self.getKeyFromMetadata(metadata)
        value = self.getValueFromMetadata(metadata)
        return (key, value)

    def getMetadata(self, strippedResult: str):
        "Parse metadata"
        metadatas = strippedResult.splitlines()
        metadatas = list(
            map(self.getKeyValFromMetadata, metadatas)
        )
        return dict(metadatas)

    def saveData(self, data: dict):
        "Save data to path"
        metadata = data['metadata']
        idstr = metadata['CDLI no.']
        imageurl = "https://cdli.ucla.edu/dl/photo/"
        imageurl = imageurl + idstr + ".jpg"
        metadata['image_url'] = imageurl
        metadata['modifications'] = []
        transliteration = data['transliteration']
        datestr = datetime.datetime.now().replace(microsecond=0,
                                                  second=0,
                                                  minute=0,
                                                  hour=0).isoformat()
        instanceIdstr = str(uuid4())
        output_name = idstr + '---time---' + datestr
        output_name = output_name + '---id---' + instanceIdstr
        metadata_output = output_name + '---type---metadata'
        transliteration_output = output_name + '---type---transliteration'
        trans_output_path = os.path.join(self.transliteration_path,
                                         transliteration_output)
        metadata_out_path = os.path.join(self.metadata_path,
                                         metadata_output)
        with open(metadata_out_path, 'w',
                  encoding='utf-8', newline='\n') as f:
            json.dump(metadata, f, ensure_ascii=False, indent=2)

        with open(trans_output_path, 'w',
                  encoding='utf-8', newline='\n') as f:
            json.dump(transliteration, f, ensure_ascii=False, indent=2)

        data['metadata_path'] = metadata_out_path
        data['transliteration_path'] = trans_output_path
        return data

    def parseResults(self):
        "Parse results"
        newresults = []
        for res in self.results:
            strippedResult = self.stripTransliteration(res)
            metadata = self.getMetadata(strippedResult)
            transliteration = self.getRawTransliteration(res)
            data = {}
            data['metadata'] = metadata
            data['transliteration'] = transliteration
            data = self.saveData(data)
            newresults.append(data)
        #
        return newresults


class ImageSaver:
    def __init__(self,
                 text_path: str,
                 metadata_path: str,
                 imsave_path: str,
                 transliteration_path: str,
                 skiplist=None,
                 ongoing=False):
        self.text_path = text_path
        self.ongoing = ongoing
        self.skiplist = skiplist
        self.metadata_path = metadata_path
        self.image_path = imsave_path
        imglob = os.path.join(self.image_path, '*')
        self.imfiles = glob.glob(imglob)
        self.transliteration_path = transliteration_path
        self.url_base = ""
        with open(self.text_path, 'r',
                  encoding='utf-8') as f:
            results = f.read()
        #
        self.parser = ResultsParser(
            results=results,
            metadata_path=self.metadata_path,
            transliteration_path=self.transliteration_path)
        self.results = self.parser.parseResults()

    def add2metadata(self, metadata_path: str,
                     key: str, val):
        "update metadata dict"
        with open(metadata_path, 'r', encoding='utf-8') as f:
            jfile = json.load(f)
        jfile[key] = val
        modif = {'modification_type': 'addition',
                 'modified_key': key,
                 'modified_value': val}
        jfile['modifications'].append(modif)
        with open(metadata_path, 'w', encoding='utf-8') as f:
            json.dump(jfile, f, ensure_ascii=False, indent=2)
        return jfile

    def getImageFromMetadata(self, metadata: dict):
        "get cdli id from metadata"
        idstr = metadata['CDLI no.']
        imageurl = metadata['image_url']
        resp = requests.get(imageurl)
        datestr = datetime.datetime.now().replace(microsecond=0,
                                                  second=0,
                                                  minute=0,
                                                  hour=0).isoformat()
        imname = idstr + '--time--' + datestr + '.jpg'
        try:
            img = Image.open(BytesIO(resp.content))
        except OSError:
            return None
        impath = os.path.join(self.image_path, imname)
        img.save(impath)
        return ('image_path',
                impath)

    def checkLocalImageFile(self, idstr: str):
        "Check if a local image file exists"
        bnames = [os.path.basename(f) for f in self.imfiles]
        checks = [bname for bname in bnames if idstr in bname]
        return len(checks) > 0

    def saveImagesFromResults(self):
        "Save images from results"
        newresults = []
        for res in self.results:
            metadata = res['metadata']
            metadata_path = res['metadata_path']
            idstr = metadata['CDLI no.']
            # pdb.set_trace()
            print('Saving image for: ', idstr)
            if self.ongoing is True and self.skiplist is not None:
                if (self.checkLocalImageFile(idstr) is True or
                        idstr in self.skiplist):
                    print('skipping ...')
                    continue
            newres = self.getImageFromMetadata(metadata)
            if newres is None:
                print('can not save image ..')
                continue
            imkey, impath = newres
            # time.sleep(1)
            metadata = self.add2metadata(metadata_path, key=imkey, val=impath)
            res['metadata'] = metadata
            self.parser.saveData(res)
            newresults.append(res)
        return newresults


class LogParser:
    "Parse a given log file and skip image ids therein"

    def __init__(self, logpath: str):
        self.logpath = logpath

    def parseLog(self):
        with open(self.logpath, 'r', encoding='utf-8') as f:
            logfile = f.readlines()
        #
        idregex = re.compile(r'P\d+')
        lines = filter(idregex.search, logfile)
        ids = [''.join(idregex.findall(linestr)) for linestr in lines]
        return ids


def main():
    "Main proceedure for the program"
    curdir = os.getcwd()

    imagedir = os.path.join(curdir, 'images')
    rawimage_dir = os.path.join(imagedir, 'raw')
    processimage_dir = os.path.join(imagedir, 'processed')

    textdir = os.path.join(curdir, 'texts')
    rawtext_dir = os.path.join(textdir, 'raw')
    transliteration_dir = os.path.join(textdir, 'transliterations')
    metadata_dir = os.path.join(textdir, 'metadata')
    print('\nHere is the current folder structure:\n')
    print('current main folder: ', curdir, '\n')
    print('  + image folder: ', imagedir)
    print('    + raw image folder: ', rawimage_dir)
    print('    + processed images folder: ', processimage_dir, '\n')
    print('  + text folder: ', textdir)
    print('    + raw text folder: ', rawtext_dir)
    print('    + transliterations folder: ', transliteration_dir)
    print('    + metadata folder: ', metadata_dir, '\n')
    print('Please enter the path of raw results text.')
    print('In most cases they are named as cdli_result_20180212.txt')
    print('Here is the list of results text in the raw text directory: \n')
    rawtextglob = os.path.join(rawtext_dir, '*.txt')
    raws = glob.glob(rawtextglob)
    for raw in raws:
        fname = os.path.basename(raw)
        print('file name: ', fname)
        print('full path: ', raw)

    print('')
    fpath = input('enter full path of downloaded result text: ')
    ongoingstr = input('Continue previous work, [y/n n by default]?: ')
    if ongoingstr == 'y':
        ongoing = True
        strinput = input('Use a previously generated log file [Y/n]: ')
        if 'y' != strinput and strinput != 'Y':
            skiplist = None
        else:
            logpath = input('Enter the full path of the log file: ')
            parser = LogParser(logpath)
            skiplist = parser.parseLog()
    else:
        ongoing = False
        skiplist = None
    imsaver = ImageSaver(text_path=fpath,
                         ongoing=ongoing,
                         skiplist=skiplist,
                         imsave_path=rawimage_dir,
                         transliteration_path=transliteration_dir,
                         metadata_path=metadata_dir)
    imsaver.saveImagesFromResults()
    print('Images are saved ...')


if __name__ == '__main__':
    main()
